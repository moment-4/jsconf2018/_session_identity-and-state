**All cljs examples kan be ran: http://app.klipse.tech/ (except when it's stated it's not possible :) )**

----

Crash course clj(s)<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="syntax"></span><span class="tag" data-tag-name="features"></span>
===================================================================================================================================================================


What is it about?<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="what"></span>
-------------------------------------------------------------------------------------------------------------

### it's a hosted language<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="what"></span><span class="tag" data-tag-name="hosted"></span>

  - clojure (clj): JVM
  - clojurescript (cljs): javascript
    - **Rule of the Least Power**: choose the least powerful language suitable for a given purpose.
    - **Atwood's Law**: any application that can be written in JavaScript, will eventually be ~~written in~~ compiled to JavaScript

      ![Minified js - assembler of the web?](./minifiedjs.png)
      
    - js was never meant to be big - we can do better than `console.log((!+[]+[]+![]).length);` and `console.log(Math.max() < Math.min());` (that's why new languages arise, like CoffeScript, Typescript... and ClojureScript)

  - clojureCLR (clr): Microsoft Common Language Runtime
  -   ...(?)
   
  
### it's more than just a(nother) hosted language<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="what"></span><span class="tag" data-tag-name="features"></span>

  - hosted
  - LISP-dialect
  - dynamic
  - immutable data stuctures
  - lazy loading
  - repl:<br />
      ![](./syntactic-abstraction-l0.png)

  - (mainly) functional with OO à la carte: state, function, identity,  value, polymorphism, inheritance<br />
    -  [Alan Kay on OO](http://userpage.fu-berlin.de/~ram/pub/pub_jf47ht81Ht/doc_kay_oop_en):

        > (I'm not against types, but I don't know of any type systems that
        > aren't a complete pain, so I still like dynamic typing.)
        > OOP to me means only messaging, local retention and protection and
        > hiding of state-process, and extreme late-binding of all things.
    
    - [OO quantified?](http://www.carlopescio.com/2012/12/ask-not-what-object-is-but.html):
    
        > min [ SUM(i:decisions) ( P(D(i)) x SUM(j:artefacts) size(A(j)) | A(j) changes on D(i) ) ]


about values, output (and parenthesis)<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="base"></span>
----------------------------------------------------------------------------------------------------------------------------------

Java:
``` {.java}
    class HelloJSConf {
       public static void main(String[] args) {
         System.out.println("42");
      }
    }
```

js:
``` {.javascript}
    console.log(Math.sqrt(parseInt("42")))
```

cljs:
``` {.clojure}
  (-> "42"
      js/parseInt
      js/Math.sqrt
      print)
```

**Note** count the parenthesis (and curly braces)!


data structures:all good things come in ~~threes~~ fours<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="data"></span>
----------------------------------------------------------------------------------------------------------------------------------------------------

-   **immutable** -&gt; persistent data structures & structural sharing ~ efficiency, equality<br />
    ![](./structural-sharing.png) 
-   **read**-able
-   **value** equality semantics


``` {.clojure}
  4544 ;; number
  \a   ;; character
  "a"  ;; string
```

``` {.clojure}
  nil  ;; nil
  'a   ;; symbol
  :a   ;; keyword
```

``` {.clojure}
  [\d 'e "f" :g]         ;; vector
  {:d 4 :e \5 :f "six"}  ;; map
  #{\d 'e "f" :g}        ;; set
  '(def g 13)            ;; list
```


code: one rule to rule them all<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="homoiconic"></span>
---------------------------------------------------------------------------------------------------------------------------------

### evaluation


``` {.clojure}
  (* 2 3 7)
  (vector? (first {1 \a, 2 \b})) ;; vector similar to map with numbers as keys but also
                                 ;; map is vector of doubles (vector with 2 elements)
```


Maps, sets and keywords are also functions (they implement `IFn`) and can be invoked:

``` {.clojure}
  (def m {:a 1, :b 2, [3 5] 8})
  (get m :b)
  (:b m)
  (m :b)
  (get m [3 5])
  (def s #{:js "conf" 2018})
  (:js s)
  (s :js)
  (s "conf")
  ("conf" s) ;; doesn't work, a string is not a function!
  (let [input :cljs]
    (if (input s)
      :input-present
      :input-absent)
```

  > If you understand how compilers work,  what's really going on 
  > is not so much that Lisp has a strange syntax as that Lisp has no syntax.           

  -- [Beating the averages - Paul Graham](http://www.paulgraham.com/avg.html)

``` {.clojure}
  (def c 8)            ;; let c=8;
  (-> '(def g 13) vec (assoc 2 17) seq eval)  ;; doesn't run in klipse: eval doesn't exist in clojurescript (too unsafe),
                                              ;; tag literals must be used to read arbitrary strings from 'outside' the system
  (/ c g) 
```

### homoiconicity<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="homoiconic"></span>


```
STRUCTURE/DATA -->    list      symbol     string
                        \         |           |
                         (     println      "42" )
                        /         |           | 
SEMANTICS/CODE --> invocation  fn-call       arg

STRUCTURE/DATA -->    list    symbol numbers
                        \       |       |
                         (      +   1 1 2 3 5 8 13 )
                        /       |       |
SEMANTICS/CODE --> invocation  fn-call args
```


``` {.clojure}
  ; let times2 = function(val) {return 2 * val;}
  (defn times-2 [val] (* 2 val)) 
  (println (times-2 4))
```

```
STRUCTURE/DATA -->     list       symbol symbol    vector       list(s)
                         \          |     |         |              |
                          (       defn   name   [arg1 arg2] (+ arg1 arg2 ) )
                         /          |     |         |              |
SEMANTICS/CODE -->   invocation   call fn-name    binding    implementation
```

-  list ~ invocation
-  vector ~ binding



| form             | java                          | js                        | clj/cljs             |
| ---------------- | ----------------------------- | ------------------------- | -------------------- |
| function/method  | public Int name(arg) <br />  {...}  | let name = function(args) <br />  {...} | (defn name [args] <br />  {...}   |
| assignment       | Int a = 3                     | let a = 3                 | (def a 3)            |
| operator         | 1 + 2                         | 1 + 2                     | (+ 1 2)              |
| method call      | "hello".trim()                | "hello".trim()            | (.trim "hello")      |
| import           | import some.pkg.*;            | import * from some.pkg    | (require 'some.pkg <br /> :refer :all)   |
| metadata         | implement meta annotation: <br />   see 'extend compiler'. <br /> <br /> @meta(...) | function addMetaData () { <br />    ... <br /> } <br /> addMetaData(obj); | (with-meta obj meta) |
| control flow     | if (valid?) {<br />  success();<br />} else {<br />  failure();<br />}                | if (valid?) {<br />  success();<br />} else {<br />  failure();<br />} | (if valid?<br />  (success)<br/>  (failure)          |
| data             | serialized objects            | {"key": "value"}<br />[1 2 {"nr": 3}]          | {:key "value", [1 1] 'has-array-key}<br /> [1 1 {:nr 3}]<br />'(2 3 5)<br />#{8 13 21}<br /> #custom-tag [arg1 arg2] |
| configuration    | <?xml ...><br />\<http\><br />   \<port\>4242\</port\><br />\</http\><br /><br /> @Configuration | {"http": {"port": 4242}}  | {:http {:port 4242}} |
| build tool setup | <?xml ...><br />\<dependencies\><br />   \<dependency\> <br />     ...<br />  \</dependency\> <br />\</dependencies\><br /> --- <br /> configurations {<br />    ...<br /> <br /> --- <br /> ...?  | {"dependencies":<br />   {...}<br /> }    | (defproject ... <br />  :dependencies <br />     [...]      |
| extend compiler  | @Target( ElementType.TYPE )<br />@Retention(RetentionPol...)<br />public @interface<br />  Immutable {} <br /><br />public class ... <br />  extends AbstractProcessor { <br />    ...<br />} <br /><br />[SimpleAnnotationProcessor](#SimpleAnnotationProcessor)<br /> <br />@Immutable <br /> class .... | ?? | (defmacro -> <br />   ...)  <br /><br /><br /><br /><br /><br /><br /><br /><br />[threading-macro](#threading-macro)<br /><br />(-> ...) |


### macros and tagged literals

from: [https://www.javacodegeeks.com/2015/09/java-annotation-processors.html](https://www.javacodegeeks.com/2015/09/java-annotation-processors.html)

> The implementation itself is pretty straightforward

<span id="SimpleAnnotationProcessor"></span>
``` {.java}
  @SupportedAnnotationTypes( "com.javacodegeeks.advanced.processor.Immutable" )
  @SupportedSourceVersion( SourceVersion.RELEASE_7 )
  public class SimpleAnnotationProcessor extends AbstractProcessor {
    @Override
    public boolean process(final Set< ? extends TypeElement > annotations, 
      final RoundEnvironment roundEnv) {

     for( final Element element: roundEnv.getElementsAnnotatedWith( Immutable.class ) ) {
       if( element instanceof TypeElement ) {
         final TypeElement typeElement = ( TypeElement )element;

         for( final Element eclosedElement: typeElement.getEnclosedElements() ) {
           if( eclosedElement instanceof VariableElement ) {
             final VariableElement variableElement = ( VariableElement )eclosedElement;

             if( !variableElement.getModifiers().contains( Modifier.FINAL ) ) {
               processingEnv.getMessager().printMessage( Diagnostic.Kind.ERROR,
                 String.format( "Class '%s' is annotated as @Immutable, 
                                 but field '%s' is not declared as final", 
                 typeElement.getSimpleName(), variableElement.getSimpleName()            
                 ) 
               );                       
             }
           }
         }
      }
      // Claiming that annotations have been processed by this processor 
      return true;
    }
  }
```

compiler extension: source code of `->` (`(-> a b c d)` is compiled to `(d (c (b a)))`) 

<span id="threading-macro"></span>
``` {.clojure}
  (defmacro ->
    "..."
    {:added "1.0"}
    [x & forms]
    (loop [x x, forms forms]
      (if forms
        (let [form (first forms)
             threaded (if (seq? form)
                        (with-meta `(~(first form) ~x ~@(next form)) (meta form))
                        (list form x))]
         (recur threaded (next forms)))
       x)))
```

reader extension with: the build-in tag `#js` (convert edn to json)

``` {.clojure}
  (require 'cljs.reader)
  (-> "#js {:author \"David Nolen\"}"
      cljs.reader/read-string
      js/JSON.stringify
      print)
```

reader extension: custom tag `#fib`

``` {.clojure}
  (defn- fib-iter []
     (->> [0 1]
          (iterate (fn [[a b]] [b (+ a b)]))
          (map first)))

  (defn fib [n]
     (take n (fib-iter)))

  (fib 8)

  (->> "{:a #fibo 8
         :b #fibo 9}"
        (cljs.reader/read-string {:readers {'fib fib}})
        cljs.pprint/pprint)
```


- **tagged literals**: date/code manipulation in read phase
- **macros**: data/code manipulation in the compile/eval phase

=&gt; tagged literals ~ reader ≡ macro's ~ evaluator

![](./syntactic-abstraction-l1.png) 


namespaces<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="namespace"></span>
-----------------------------------------------------------------------------------------------------------

namespaces \~ (ES6) modules or `goog.module`

``` {.clojure}
  (ns data.map)
  (def m {:a 1, :b 2})

  (ns result.calc)
    (:require [data.map :refer [m] :rename {m dm-m}]))
  (let [new-map (assoc m :c 3)]
    (* (:c new-map) 2))
```

State management<span class="tag" data-tag-name="cljs"></span><span class="tag" data-tag-name="state"></span><span class="tag" data-tag-name="value"></span>
============================================================================================================================================================

Identity, state and immutability
--------------------------------

![State and identity in clojurescript](./state-mgmt-clj.png)

from: [http://lambdax.io/blog/posts/2016-04-18-state-management-in-clojure.html](http://lambdax.io/blog/posts/2016-04-18-state-management-in-clojure.html)

![Epochal time model: identity is not a state, it has(!) a state](./epochal-time-model.jpg)

from: [http://www.javiersaldana.com/tech/2014/10/15/mental-models-for-concurrency.html](http://www.javiersaldana.com/tech/2014/10/15/mental-models-for-concurrency.html)


Values don't change
-------------------

*Note:* they do in languages with mutable data structures, like js :)

``` {.javascript}
  var arr = [ 5, 3, 2 ];

  var no_side_effects = function(par) {
     return Math.max() < par < Math.min();
  }

  var with_side_effects = function(par) {
     par.sort();
     return Math.max();
  }

  console.log(no_side_effects(Math.min()));
  console.log(arr);
  console.log(with_side_effects(arr));
  console.log(arr);
```


- **var**: immutable data structure... 
           unless you define it as `^:dynamic` =\>
    - dynamic scope (vs lexical scope)
    - convention to use earmuffs `*var-name*`
    - DON'T DO THIS (unless you really know what you're doing)!
    
``` {.clojure}
  (def ^:dynamic *arr* [5 3 2])
        
  (defn dynamic-scope []
    (println *arr*))
        
  (dynamic-scope)
  (binding [*arr* 45]
    (dynamic-scope))
  (dynamic-scope)
 ```

**Note** but mutable state is possible: it wraps immutable state in a container where you can swap or (re)set the immutable value work towards
 

- **atom**: container holding an immutable value:
    -   needs `deref` to get the value
    -   `swap!` and `set!`
    -   support for listeners (watches) and validators

``` {.clojure}
  (def mutable (atom '(8 5 3)))
  (str "mutable" mutable)
  (str "(deref mutable):" (deref mutable))
  (apply str (repeat "-" 80))
  (str "@mutable before swap: " @mutable)
  (swap! mutable conj 13)
  (str "@mutable after swap: " @mutable)
  (reset! mutable 13)
  (str "@mutable after reset: " @mutable)
  
  
  (def positive (atom 1 :validator #(> % 0)))
  (add-watch positive
             :print-some-data (fn [key atom-ref old-val new-val] 
                               (println "["key"] changing " 
                                        atom-ref 
                                        ":\n\t" 
                                        old-val 
                                        "->" 
                                        new-val)))
  @positive
  (swap! positive * 5 6)
  @positive
  (reset! positive 6)
  @positive
  (try
    (swap! positive * 5 -6)
    (catch :default e
      (println e)))
  @positive
  (reset! positive 6)
  @positive
```
 
- ~~**agent**: like atom, but async~~
- ~~**refs**: coordinated mutations~~


Result of cljs compilation: demo
================================

http://app.klipse.tech/

``` {.clojure}
(deftype ConferenceType [year name speakers sessions)
(def jsconf2018-type (ConferenceType. 2018 "jsconf" ["Kurt Sys"] [{:speaker "Kurt Sys" :title "Whatever clojurescript"}]))
(.-year jsconf2018-type)

(defrecord ConferenceRecord [year name speakers sessions])
(def jsconf2018-record (ConferenceRecord. 2018 "jsconf" ["Kurt Sys"] [{:speaker "Kurt Sys" :title "Whatever clojurescript"}]))
(.-year jsconf2018-record)
(:year jsconf2018-record)
```

[https://clojure.org/reference/datatypes](https://clojure.org/reference/datatypes)

Cool Stuff
==========

  * Polymorphism: protocols and multimethods(!)
  * Typing: defrecord, deftype, `clojure.spec`
  * Transducers

    > It is better to have 100 functions operate on ~~one~~ no data structure than
    > than 10 functions on 10 data structures.
    (adapted from Alan Perlis)
 
  * Libraries

| lib          |   usage                                 |
| ------------ | --------------------------------------- |
| core.async   | golang as a library, implementation of SCP (C.A.R Hoare) |
| core.match   | pattern matching |
| clojure.spec | describing your data as code, just enough typing without type system |
| reagent      | react wrapper for cljs |
| re-frame     | opinionated pattern to write SPAs (uses react under the hood) |
| datascript   | immutable in-memory database and datalog engine (meant to run inside a browser) |
| cljs-ajax    |  ... |
| cljs-http    |  ... |


